var roleMiner = {

    /** @param {Creep} creep **/
    run: function(creep) {

        var sources = creep.room.find(FIND_SOURCES);
        var closestSource = creep.pos.findClosestByPath(FIND_SOURCES);
        //console.log(closestSource);

        if(creep.harvest(closestSource) == ERR_NOT_IN_RANGE) {
            creep.moveTo(closestSource, {visualizePathStyle: {stroke: '#ffaa00'}});
        }
        else
        {
          creep.harvest(closestSource);
        }
	}
};

module.exports = roleMiner;
