var respawn = {

  run:function()
  {
    for(var i in Memory.creeps) {
      if(!Game.creeps[i]) {
          delete Memory.creeps[i];
      }
    }
    var needSpawn = false;
    var spawned = false;

    for(var role in Memory.roles){

      var targets = Game.spawns['Spawn1'].room.find(FIND_CONSTRUCTION_SITES);
      if(role ==3 && targets.length==0) break;
      
      var currentRole = Memory.roles[role];

      var groupCreep = _.filter(Game.creeps,(creep) => creep.memory.role == currentRole.name);
      var nbElement = groupCreep.length;

      for(var creep in groupCreep)
      {
        var current = groupCreep[creep];
        if (current.tickToLive <15) needSpawn = true;
      }
      if(nbElement < currentRole.nb) needSpawn = true;
      if(needSpawn && Game.spawns['Spawn1'].spawning == null){

        var id=0;
        var name = currentRole.name + id;
        if(Game.spawns['Spawn1'].spawnCreep(currentRole.body, name, {dryRun : true})!= -6){
          while(Game.spawns['Spawn1'].spawnCreep(currentRole.body, name, {memory: {role: currentRole.name }})==-3){
              //console.log(name + " : Impossible");
              id++;
              name = currentRole.name + id;
          }
          spawned=true;
        }
        break;
      }
    }
    if(!spawned && Memory.nbSourceToMine < Memory.sources.length){
      console.log("Adding 1 energy source");
      Memory.roles[0].nb = Memory.roles[0].nb + Memory.sources[Memory.nbSourceToMine].nbWorker;
      Memory.roles[1].nb = Memory.roles[1].nb + 1;
      Memory.roles[2].nb = Memory.roles[2].nb + Memory.sources[Memory.nbSourceToMine].nbWorker;
      Memory.nbSourceToMine++;
    }

  }
}

module.exports = respawn;
