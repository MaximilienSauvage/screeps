var roleUpgrader = {

    /** @param {Creep} creep **/
    run: function(creep) {

        if(creep.memory.upgrading && creep.carry.energy == 0) {
            creep.memory.upgrading = false;
	    }
	    if(!creep.memory.upgrading && creep.carry.energy == creep.carryCapacity) {
	        creep.memory.upgrading = true;
	        creep.say('⚡ upgrade');
	    }

	    if(creep.memory.upgrading) {
            if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller, {visualizePathStyle: {stroke: '#ffffff'}});
            }
        }
        else {

          for(var structType = Memory.refillPrio.length; structType>=0;structType--)
          {
            var currentStruct = Memory.refillPrio[structType];
            var targets = creep.room.find(FIND_STRUCTURES, {filter: {structureType: currentStruct}});

            if(targets.length > 0) {
              var thisType = _.filter(targets,(Structure) => Structure.store.getUsedCapacity(RESOURCE_ENERGY)>0);

              if(thisType.length > 0){
                if(creep.withdraw(thisType[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                   creep.moveTo(thisType[0]);
                }
              }
            }
          }
      }
	}
};

module.exports = roleUpgrader;
