var roleBuilder = {

    /** @param {Creep} creep **/
    run: function(creep) {

	    if(creep.memory.building && creep.carry.energy == 0) {
            creep.memory.building = false;
	    }
      //if(creep.memory.income != undefined){
        if((!creep.memory.building && creep.carry.energy == creep.carryCapacity)){ //|| creep.memory.income.store.getUsedCapacity(RESOURCE_ENERGY) == 0){
  	        creep.memory.building = true;
            creep.memory.income = undefined;
  	        //creep.say('🚧 build');
  	    }
    //  }

	    if(creep.memory.building) {
        //building mode
        for(var structType in Memory.buildPrio)
        {
          var currentStruct = Memory.buildPrio[structType];
          var targets = creep.room.find(FIND_CONSTRUCTION_SITES, {filter: {structureType: currentStruct}});

          if(targets.length > 0) {
            if(creep.build(targets[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(targets[0], {visualizePathStyle: {stroke: '#ffffff'}});
            }
          }
        }
        //Repair mode

      }
	    else {

        if(creep.memory.income == undefined){
          for(var structType = Memory.refillPrio.length; structType>=0;structType--)
          {
            var currentStruct = Memory.refillPrio[structType];
            var targets = creep.room.find(FIND_STRUCTURES, {filter: {structureType: currentStruct}});

            //console.log(currentStruct + "  : " + targets.length);
            if(targets.length > 0) {
              var thisType = _.filter(targets,(Structure) => Structure.store.getUsedCapacity(RESOURCE_ENERGY)>0);

              if(thisType.length > 0){
                  creep.memory.income = thisType[0];
                }
              }
            }
        }else{
          if(creep.withdraw(Game.getObjectById(creep.memory.income.id), RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
             creep.moveTo(Game.getObjectById(creep.memory.income));
        }
      }
    }
	}
};

module.exports = roleBuilder;
