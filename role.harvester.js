var roleHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {

      //Assignation d'une source en memoire si elle n'existe pas
      if(creep.memory.linkedSource == undefined){
        var harvs = _.filter(Game.creeps,(creep) => creep.memory.role == 'harvester')
        var counter = 0;
        var tmp=0;
        var stopped = false;

        for(var s in harvs){
          if(tmp == Memory.nbSourceToMine){
              stopped= true;
              break;
          }
          var fs = harvs[s].memory.linkedSource;
          var nextFs;
          if (harvs[s+1] !=undefined)nextFs = harvs[s+1].memory.linkedSource;
          else nextFs = "Not this time";

          if (fs != nextFs){
            if(counter <= Memory.sources[tmp].nbWorker) break;
            tmp++;
            counter=0;
          }
        }
        if(!stopped){
          creep.memory.linkedSource = Memory.sources[tmp].source;
        }
      }


	    if(creep.carry.energy < creep.carryCapacity) {
            var sources = creep.room.find(FIND_SOURCES);
            if(creep.harvest(Game.getObjectById(creep.memory.linkedSource.id)) == ERR_NOT_IN_RANGE) {
                creep.moveTo(Game.getObjectById(creep.memory.linkedSource.id), {visualizePathStyle: {stroke: '#ffaa00'}});
            }
        }
        else {
            var carries = _.filter(Game.creeps,(creep) => creep.memory.role == 'carry');
            var harvs = _.filter(Game.creeps,(creep) => creep.memory.role == 'harvester');
            if(carries.length < harvs.length/2){
              for(var structType in Memory.refillPrio)
              {
                var currentStruct = Memory.refillPrio[structType];
                var targets = creep.room.find(FIND_STRUCTURES, {filter: {structureType: currentStruct}});

                if(targets.length > 0) {
                  var thisType = _.filter(targets,(Structure) => Structure.store.getFreeCapacity(RESOURCE_ENERGY)>0);
                  //console.log(thisType[0]);
                  if(thisType.length > 0){

                    if(creep.transfer(thisType[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                      creep.say(thisType[0].structureType);
                      creep.moveTo(thisType[0]);
                    }
                  }
                }
              }
            }
        }
	}
};

module.exports = roleHarvester;
