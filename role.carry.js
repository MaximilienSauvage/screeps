var roleCarry = {

    /** @param {Creep} creep **/
    run: function(creep) {

        var miners = _.filter(Game.creeps, (creep) => creep.memory.role == 'harvester' && !creep.spawning);

        if (creep.store.getFreeCapacity(RESOURCE_ENERGY) > creep.store.getCapacity()/2 )
        {
          var chosenOne = null
        //  creep.say("Coming for you");
          for(var mineCr in miners)
          {
            var mineCreep = miners[mineCr];
            if(mineCreep.store.getFreeCapacity(RESOURCE_ENERGY) < mineCreep.store.getCapacity(RESOURCE_ENERGY)/4)
            {
              chosenOne = mineCreep;
              break;
            }
          }
          if (chosenOne != null){
            if(chosenOne.transfer(creep, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE){
                creep.moveTo(chosenOne);
              }
          }

        }else{
          //creep.say("Going back");
          for(var structType in Memory.refillPrio)
          {
            var currentStruct = Memory.refillPrio[structType];
            var targets = creep.room.find(FIND_STRUCTURES, {filter: {structureType: currentStruct}});

            if(targets.length > 0) {
              var thisType = _.filter(targets,(Structure) => Structure.store.getFreeCapacity(RESOURCE_ENERGY)>0);

              if(thisType.length > 0){
                if(creep.transfer(thisType[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                  creep.moveTo(thisType[0]);
                }
              }
            }
          }
      }
	}
};

module.exports = roleCarry;
