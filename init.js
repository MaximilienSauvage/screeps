var init = {

  run:function()
  {
    if(!Memory.init){
      Memory.sources = [];
      //trouvez les sources de la map
      var sources = Game.spawns["Spawn1"].room.find(FIND_SOURCES);
      sources = sources.sort(function(a,b){
        return a.pos.getRangeTo(Game.spawns["Spawn1"]) - b.pos.getRangeTo(Game.spawns["Spawn1"]);
      });
      //Donner aux sources le nombre de place dispo pour roleHarvester
      var nbFree = [];
      var harvTot= 0;
      for(var s in sources)
      {
        nbFree[s] = 0;
        var currentS = sources[s];
        for (var i =-1;i<=1;i++){
          for(var j=-1;j<=1;j++){
            var tab = currentS.room.lookAt(currentS.pos.x+i,currentS.pos.y+j);

            if(tab[0].type == 'terrain' && (tab[0].terrain == "plain" || tab[0].terrain == "swamp")){
              nbFree[s]++;
              harvTot++;
            }
          }
        }
        Memory.sources[s] = {source : sources[s], nbWorker : nbFree[s]};
      }
      Memory.nbSourceToMine = 1;
      console.log("Harvester to build : " + harvTot);
      //set les Priorités des differents build et refill d'energie.
      Memory.buildPrio = [STRUCTURE_EXTENSION, STRUCTURE_WALL, STRUCTURE_RAMPART, STRUCTURE_TOWER, STRUCTURE_ROAD, STRUCTURE_CONTAINER];
      Memory.refillPrio = [STRUCTURE_SPAWN, STRUCTURE_EXTENSION, STRUCTURE_STORAGE, STRUCTURE_CONTAINER,STRUCTURE_TOWER];
      Memory.roles = [
                      {name: 'harvester',body: [WORK,CARRY,MOVE],nb:nbFree[0]},
                      {name: 'upgrader',body: [WORK,CARRY,MOVE],nb:1},
                      {name: 'carry',body: [CARRY,MOVE,MOVE],nb:nbFree[0]},
                      {name: 'builder',body: [WORK,CARRY,MOVE],nb:1}
                    ];
      Memory.init = true;
    }
  }
};

module.exports = init;
