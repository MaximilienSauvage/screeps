//Fonctions principales
var init = require('init');
var respawn = require('respawn');
var taskManager = require('taskManager');

//roles des screeps
var roleHarvester = require('role.harvester');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');
var roleCarry = require('role.carry');
var roleRepairer = require('role.repairer')


module.exports.loop = function () {

  init.run();
  respawn.run();

  for(var name in Game.creeps) {
  var creep = Game.creeps[name];
  if(creep.memory.role == 'harvester') {
    roleHarvester.run(creep);
  }
  if(creep.memory.role == 'upgrader') {
    roleUpgrader.run(creep);
  }
  if(creep.memory.role == 'builder') {
    roleBuilder.run(creep);
  }
  if(creep.memory.role == 'carry'){
    roleCarry.run(creep);
  }
  if(creep.memory.role == 'repairer')
  {
    roleRepairer.run(creep);
  }
}
}
