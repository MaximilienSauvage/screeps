var roleRepairer = {

  run :function(creep){
      if(creep.memory.repair && creep.carry.energy == 0) {
          creep.memory.repair = false;
    }
    if(!creep.memory.repair && creep.carry.energy == creep.carryCapacity) {
        creep.memory.repair = true;
        //creep.say('🚧 repairing');
    }

    if (creep.memory.repair)
    {
      for(var structType in Memory.buildPrio)
      {

        var currentStruct = Memory.buildPrio[structType];
        if (currentStruct.structureType != STRUCTURE_WALL){
          var targets = creep.room.find(FIND_STRUCTURES, {filter: {structureType: currentStruct}});

          if(targets.length > 0) {
            if(creep.repair(targets[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(targets[0], {visualizePathStyle: {stroke: '#ffffff'}});
            }
          }
        }else continue;
      }
    }
    else{
      for(var structType = Memory.refillPrio.length; structType>=0;structType--)
      {
        var currentStruct = Memory.refillPrio[structType];
        var targets = creep.room.find(FIND_STRUCTURES, {filter: {structureType: currentStruct}});

        if(targets.length > 0) {
          var thisType = _.filter(targets,(Structure) => Structure.store.getUsedCapacity(RESOURCE_ENERGY)>0);

          if(thisType.length > 0){
            if(creep.withdraw(thisType[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
               creep.moveTo(thisType[0]);
            }
          }
        }
      }
    }
  }
};

module.exports = roleRepairer;
